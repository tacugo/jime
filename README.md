# JIME

> JavaScript IME

[![NPM Version][npm-image]][npm-url]

🚧 Please note that this library is still WIP and not yet intended for use in production! ⚠

JIME is a simple Input Method Editor for JavaScript that can be used to convert any input to any output, as long as the conversion path is well defined. Most common use cases are to convert latin script into other scripts, e.g. into Japanese hiragana or katakana scripts. JIME's limitation is that the conversion has to be definite, i.e. it can't handle multiple possible results as is required to type for example Chinese characters.

## Install

```bash
npm install jime
```

## Usage

```js
import { Jime } from 'jime';
import { kana } from 'jime/maps/japanese';

const jime = new Jime(kana);

console.log(jime.scan('nihongohaomoshiroidesu')); // にほんごはおもしろいです
```

```js
import { Jime } from 'jime';
import { pinyin } from 'jime/maps/chinese';

const jime = new Jime(pinyin);

console.log(jime.scan('wo3 jue2de xue2xi2 zhong1wen2 hen3 you3yi4si')); // wǒ juéde xuéxí zhōngwén hěn yǒuyìsi
```

## License

ISC

[npm-image]: https://img.shields.io/npm/v/jime.svg
[npm-url]: https://npmjs.org/package/jime
