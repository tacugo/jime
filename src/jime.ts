import { InputTree, schema } from './input-tree';

export interface JimeOptions {
    ajv?: import('ajv').Ajv;
}

enum Composition {
    None = 'composition-none',
    Start = 'composition-start',
    Update = 'composition-update',
    End = 'composition-end',
}

interface CompositionEntry {
    node: InputTree;
    path: string;
    value: string;
    type: Composition;
    isChain?: boolean;
}

export class Jime {
    private root: InputTree;
    private history: CompositionEntry[][] = [[]];
    private composition = this.history[0];
    private node: InputTree;
    private path = '';

    get value() {
        return this.history
            .filter(([composition]) => composition) // ignore empty sequence
            .map(([composition]) => {
                if (composition.type === Composition.End) {
                    return composition.value;
                }
                return `${composition.path}${composition.value}`;
            })
            .reverse()
            .join('');
    }

    constructor(inputTree: InputTree, options: JimeOptions = {}) {
        if (options.ajv) {
            if (!options.ajv.validate(schema, inputTree)) {
                throw new Error(options.ajv.errorsText(options.ajv.errors));
            }
        }

        this.root = this.node = inputTree;
    }

    scan(input: string) {
        const jime = new Jime(this.root);
        for (const char of input) {
            jime.add(char);
        }
        return jime.value;
    }

    add(char: string): string;
    add(char: unknown) {
        if (typeof char !== 'string' || char.length !== 1) {
            throw new Error(`Expected single character string input, got: ${char}`);
        }

        const node = this.node[char];
        const wildcard = this.node[''];

        if (node) {
            if (typeof node === 'string') {
                this.compositionLeaf(node, char);
            } else {
                const type = this.node === this.root ? Composition.Start : Composition.Update;
                this.compositionSubtree(node, char, type);
            }
        } else if (wildcard && typeof wildcard === 'string') {
            this.compositionLeaf(wildcard, char);
        } else {
            this.path = '';
            this.node = this.root;

            if (this.root[char]) {
                this.composition = [];
                this.history.unshift(this.composition);
                this.add(char);
            } else {
                this.compositionIsolate(char);
            }
        }

        return this.value;
    }

    delete(): string {
        if (this.history[0].length === 0) {
            this.history.shift();
            this.composition = this.history[0];
            return this.delete();
        }

        const entry = this.history[0].shift();

        if (!entry) {
            throw new Error(`unexpected state`);
        }

        if (entry.isChain) {
            return this.delete();
        }

        this.node = entry.node;
        this.path = entry.path;

        return this.value;
    }

    private getChain(leaf: string, input: string): [string, string?] {
        const match = /\{(.?)\}$/.exec(leaf);

        if (match) {
            return [leaf.replace(match[0], ''), match[1] || input];
        }

        return [leaf];
    }

    private compositionLeaf(leaf: string, input: string) {
        const [value, chainedChar] = this.getChain(leaf, input);

        this.composition.unshift({
            node: this.node,
            path: this.path,
            value,
            type: Composition.End,
        });

        this.composition = [];
        this.history.unshift(this.composition);

        this.path = '';
        this.node = this.root;

        if (!chainedChar) {
            return;
        }

        const chainedNode = this.node[chainedChar];

        if (!chainedNode) {
            return this.compositionIsolate(chainedChar);
        }

        if (typeof chainedNode === 'object') {
            this.compositionSubtree(chainedNode, chainedChar, Composition.Start, true);
        } else {
            this.compositionLeaf(chainedNode, chainedChar);
        }
    }

    private compositionSubtree(
        tree: InputTree,
        input: string,
        type: Composition.Start | Composition.Update,
        chain?: boolean,
    ) {
        this.composition.unshift({
            node: this.node,
            path: this.path,
            value: input,
            type,
            isChain: chain,
        });

        this.path = `${this.path}${input}`;
        this.node = tree;
    }

    private compositionIsolate(input: string) {
        this.composition.unshift({
            node: {},
            path: '',
            value: input,
            type: Composition.None,
        });

        this.composition = [];
        this.history.unshift(this.composition);
    }
}
