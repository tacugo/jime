import { JSONSchema7 } from 'json-schema';

export interface InputTree {
    [char: string]: string | InputTree | undefined;
}

export const schema: JSONSchema7 = {
    definitions: {
        inputTree: {
            type: 'object',
            patternProperties: {
                '^[^\\s]$': {
                    oneOf: [
                        {
                            type: 'string',
                        },
                        {
                            $ref: '#/definitions/inputTree',
                        },
                    ],
                },
                '^$': {
                    type: 'string',
                },
            },
        },
    },
    $ref: '#/definitions/inputTree',
};
