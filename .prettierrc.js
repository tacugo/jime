module.exports = {
    printWidth: 120,
    tabWidth: 4,
    semi: true,
    singleQuote: true,
    quoteProps: 'consistent',
    trailingComma: 'all',
    arrowParens: 'always',
}