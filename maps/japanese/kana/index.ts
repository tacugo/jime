import { InputTree } from 'index';

import { hiragana } from './hiragana';
import { katakana } from './katakana';

export const kana: InputTree = {
    ...hiragana,
    ...katakana,
};
