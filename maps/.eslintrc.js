module.exports = {
    extends: [
        '../.eslintrc.js',
        'prettier',
        'prettier/@typescript-eslint',
        'plugin:prettier/recommended', // last
    ],
    rules: {
        'prettier/prettier': ['error', { quoteProps: 'preserve' }],
    }
}