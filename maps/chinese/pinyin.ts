import { InputTree } from 'index';

function tones(final: string): InputTree {
    const marks: Record<string, string[]> = {
        'a': ['ā', 'á', 'ǎ', 'à'],
        'e': ['ē', 'é', 'ě', 'è'],
        'i': ['ī', 'í', 'ǐ', 'ì'],
        'o': ['ō', 'ó', 'ǒ', 'ò'],
        'u': ['ū', 'ú', 'ǔ', 'ù'],
        'ü': ['ǖ', 'ǘ', 'ǚ', 'ǜ'],
    };

    const match = /[ae]/.exec(final) || /o/.exec(final) || /[iuü][^aeiouü]*$/.exec(final);

    if (!match) {
        throw new Error();
    }

    const vowel = match[0].charAt(0);

    return {
        '1': final.replace(vowel, marks[vowel][0]),
        '2': final.replace(vowel, marks[vowel][1]),
        '3': final.replace(vowel, marks[vowel][2]),
        '4': final.replace(vowel, marks[vowel][3]),
        'r': {
            '1': `${final.replace(vowel, marks[vowel][0])}r`,
            '2': `${final.replace(vowel, marks[vowel][1])}r`,
            '3': `${final.replace(vowel, marks[vowel][2])}r`,
            '4': `${final.replace(vowel, marks[vowel][3])}r`,
            '': `${final}r{}`,
        },
        '': `${final}{}`,
    };
}

export const pinyin = {
    'a': {
        ...tones('a'),
        'i': tones('ai'),
        'o': tones('ao'),
        'n': {
            ...tones('an'),
            'g': tones('ang'),
        },
    },
    'e': {
        ...tones('e'),
        'i': tones('ei'),
        'n': {
            ...tones('en'),
            'g': tones('eng'),
        },
    },
    'o': {
        ...tones('o'),
        'u': tones('ou'),
        'n': {
            'g': tones('ong'),
        },
    },
    'u': {
        ...tones('u'),
        'a': 'u{}',
        'e': 'u{}',
        'i': 'u{}',
        'o': 'u{}',
    },
    'i': {
        ...tones('i'),
        'a': 'i{}',
        'e': 'i{}',
        'o': 'i{}',
        'u': 'i{}',
    },
    'ü': {
        ...tones('ü'),
        'e': 'ü{}',
    },
    'v': {
        ...tones('ü'),
        'e': 'ü{}',
    },
};
