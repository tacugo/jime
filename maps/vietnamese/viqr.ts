import { InputTree } from 'index';

function base(char: string): InputTree {
    return {
        ' ': char,
        '-': char,
        '\\': {
            '': `${char}{}`,
        },
        '': `${char}{}`,
    };
}

function tones(z: string, s: string, f: string, r: string, x: string, j: string): InputTree {
    return {
        ...base(z),
        "'": s,
        '`': f,
        '?': r,
        '~': x,
        '.': j,
    };
}

export const VIQR: InputTree = {
    'a': {
        ...tones('a', 'á', 'à', 'ả', 'ã', 'ạ'),
        '(': tones('ă', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ'),
        '^': tones('â', 'ấ', 'ầ', 'ẩ', 'ẫ', 'ậ'),
    },
    'A': {
        ...tones('A', 'Á', 'À', 'Ả', 'Ã', 'Ạ'),
        '(': tones('Ă', 'Ắ', 'Ằ', 'Ẳ', 'Ẵ', 'Ặ'),
        '^': tones('Â', 'Ấ', 'Ầ', 'Ẩ', 'Ẫ', 'Ậ'),
    },
    'd': {
        ...base('d'),
        'd': 'đ',
    },
    'D': {
        ...base('D'),
        'd': 'Đ',
        'D': 'Đ',
    },
    'e': {
        ...tones('e', 'é', 'è', 'ẻ', 'ẽ', 'ẹ'),
        '^': tones('ê', 'ế', 'ề', 'ẻ', 'ẽ', 'ệ'),
    },
    'E': {
        ...tones('E', 'É', 'È', 'Ẻ', 'Ẽ', 'Ẹ'),
        '^': tones('Ê', 'Ế', 'Ề', 'Ể', 'Ễ', 'Ệ'),
    },
    'i': tones('i', 'í', 'ì', 'ỉ', 'ĩ', 'ị'),
    'I': tones('I', 'Í', 'Ì', 'Ỉ', 'Ĩ', 'Ị'),
    'o': {
        ...tones('o', 'ó', 'ò', 'ỏ', 'õ', 'ọ'),
        '^': tones('ô', 'ố', 'ồ', 'ổ', 'ỗ', 'ộ'),
        '+': tones('ơ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ'),
    },
    'O': {
        ...tones('O', 'Ó', 'Ò', 'Ỏ', 'Õ', 'Ọ'),
        '^': tones('Ô', 'Ố', 'Ồ', 'Ổ', 'Ỗ', 'Ộ'),
        '+': tones('Ơ', 'Ớ', 'Ờ', 'Ở', 'Ỡ', 'Ợ'),
    },
    'u': {
        ...tones('u', 'ú', 'ù', 'ủ', 'ũ', 'ụ'),
        '+': tones('ư', 'ứ', 'ừ', 'ử', 'ữ', 'ự'),
    },
    'U': {
        ...tones('U', 'Ú', 'Ù', 'Ủ', 'Ũ', 'Ụ'),
        '+': tones('Ư', 'Ứ', 'Ừ', 'Ử', 'Ữ', 'Ự'),
    },
    'y': tones('y', 'ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ'),
    'Y': tones('Y', 'Ý', 'Ỳ', 'Ỷ', 'Ỹ', 'Ỵ'),
};
